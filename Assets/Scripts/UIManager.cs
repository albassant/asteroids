﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour
{
	public Text scoreLabel;
	public GameObject gameOverPanel;

	public AnimationCurve animationCurve;

	private GameManager gameManager;

	void Start()
	{
		gameManager = FindObjectOfType<GameManager>();
		if (gameManager == null) Debug.LogAssertion("GameManager object is missing");

		gameManager.OnGameIsOver += () => StartCoroutine(ShowGameOverPanel());
		gameManager.OnScoreUpdated += UpdateScore;
		gameOverPanel.transform.localScale = Vector3.zero;
	}

	public void OnStartButton()
	{
		StartCoroutine(HideGameOverPanel());
	}

	IEnumerator ShowGameOverPanel()
	{
		gameOverPanel.SetActive(true);

		float curTime = 0f;
		float animTime = 0.3f;

		Transform panelTransform = gameOverPanel.transform;
		while (curTime < animTime)
		{
			curTime += Time.deltaTime;
			float t = curTime / animTime;

			panelTransform.localScale = Vector3.one * animationCurve.Evaluate(t);

			yield return null;
		}
	}

	IEnumerator HideGameOverPanel()
	{
		float curTime = 0f;
		float animTime = 0.3f;

		Transform panelTransform = gameOverPanel.transform;
		while (curTime < animTime)
		{
			curTime += Time.deltaTime;
			float t = curTime / animTime;

			panelTransform.localScale =  Vector3.one * (1f - animationCurve.Evaluate(t));

			yield return null;
		}

		gameOverPanel.SetActive(false);
		gameManager.StartGame();
	}

	void UpdateScore(int newScore)
	{
		scoreLabel.text = "Score: " + newScore;
	}
}
