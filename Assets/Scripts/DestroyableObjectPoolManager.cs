﻿using UnityEngine;
using System.Collections;

public class DestroyableObjectPoolManager : MonoBehaviour
{
	public GameObject[] ObjectPrefabs;
	public int MaxInstances = 10;

	protected GameObjectPool objectPool = null;

	void Awake()
	{
		objectPool = new GameObjectPool(MaxInstances);
	}

	public virtual DestroyableObject SpawnObject(Vector3 spawnPosition, Quaternion spawnRotation)
	{
		int prefabIdx = ObjectPrefabs.Length > 1 ? Random.Range(0, ObjectPrefabs.Length) : 0;
		objectPool.prefab = ObjectPrefabs[prefabIdx];

		GameObject obj = objectPool.Instantiate(spawnPosition, spawnRotation);
		obj.transform.SetParent(this.transform);

		var destrObj = obj.GetComponent<DestroyableObject>();
		destrObj.OnCollision += HandleDestruction;
		destrObj.OnLeftVisibleArea += HandleLeftVisibleArea;

		return destrObj;
	}

	protected virtual void UtilizeObject(DestroyableObject obj)
	{
		obj.OnLeftVisibleArea -= HandleLeftVisibleArea;
		obj.OnCollision -= HandleDestruction;
		objectPool.Push(obj.gameObject);
	}

	protected virtual void HandleLeftVisibleArea(DestroyableObject obj)
	{
		UtilizeObject(obj);
	}

	protected virtual void HandleDestruction(DestroyableObject obj)
	{
		UtilizeObject(obj);
	}

	public void UtilizeVisibleObjects()
	{
		var objects = FindObjectsOfType<DestroyableObject>();

		foreach (var obj in objects)
		{
			if (obj != null)
			{
				UtilizeObject(obj);
			}
		}
	}
}
