﻿using UnityEngine;
using System.Collections;

public class Asteroid : DestroyableObject 
{
	public System.Action<Asteroid> OnSpawnParts;

	public float minMovementSpeed = 1f;
	public float maxMovementSpeed = 5f;
	public float tumble = 5f;

	public int hitScore = 15;

	public int Score { get { return hitScore * maxDamage; } }

	public GameObject explosionPrefab;

	private int maxDamage;
	public int MaxDamage
	{
		get { return maxDamage; }
		set
		{
			maxDamage = value;
			transform.localScale = Vector3.one * maxDamage;
		}
	}

	private int currentDamage;

	void OnEnable()
	{
		Vector3 target = MainCamera.Instance.InvertPosition(rigidbody.position);
		Vector3 dir = target - rigidbody.position;

		rigidbody.velocity = dir.normalized * Random.Range(minMovementSpeed, maxMovementSpeed);
		rigidbody.angularVelocity = Random.insideUnitSphere * tumble;

		currentDamage = 0;
	}

	protected override void HandleDamage()
	{
		currentDamage++;
		if (currentDamage == maxDamage)
		{
			if (maxDamage > 1 && OnSpawnParts != null)
			{
				OnSpawnParts(this);
			}
			PlayExplosion();
			base.HandleDamage();
		}
	}

	void PlayExplosion()
	{
		Instantiate(explosionPrefab, transform.position, transform.rotation);
	}

	void OnBecameInvisible()
	{
		rigidbody.position = MainCamera.Instance.InvertPosition(rigidbody.position);
	}
}
