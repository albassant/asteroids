﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	public System.Action OnDied;

    public enum ERotationDirection
    {
        Left,
        Right,
        None,
    }
    private ERotationDirection rotationDirection = ERotationDirection.None;

    public float angularSpeed;
	public float slowDownPerSecond = 0.5f;
	public float accelerationSpeed = 2f;
    public float fireRate = 0.3f;

	public Transform ShotSpawnPoint;

    public GameObject enginesVFX;
	public GameObject explosionVFX;

    private float nextFire = 0f;
	private AudioSource shotSFX;

	private DestroyableObjectPoolManager projectileManager;
	Rigidbody rigidbody;

    void Start()
    {
		projectileManager = GetComponent<DestroyableObjectPoolManager>();
		if (projectileManager == null) Debug.LogError("DestroyableObjectPoolManager component not found");

		rigidbody = GetComponent<Rigidbody>();
		if (rigidbody == null) Debug.LogError("Rigidbody component not found");

		shotSFX = GetComponent<AudioSource>();
		if (shotSFX == null) Debug.LogError("AudioSource component not found");
    }


    void FixedUpdate()
    {
        if (rotationDirection != ERotationDirection.None)
        {
            float dir = rotationDirection == ERotationDirection.Right ? 1f : - 1f;
			rigidbody.AddTorque(transform.up * angularSpeed * dir, /*ForceMode.Force*/ForceMode.VelocityChange);
        }

        if (accelerated)
        {
			rigidbody.AddForce(transform.forward * accelerationSpeed, /*ForceMode.Impulse*/ForceMode.VelocityChange);
        }

		if (!MainCamera.Instance.IsInsideVisibleArea(rigidbody.position))
		{
			rigidbody.position = MainCamera.Instance.InvertPosition(rigidbody.position);
		}
    }

    public void Rotate(ERotationDirection dir)
    {
        rotationDirection = dir;
    }

    public void Fire()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
			projectileManager.SpawnObject(ShotSpawnPoint.position, ShotSpawnPoint.rotation);
			shotSFX.Play();
        }
    }

    bool accelerated = false;
    public void Accelerate(bool pressed)
    {
        enginesVFX.SetActive(pressed);
        accelerated = pressed;
    }

	void OnTriggerEnter(Collider other)
	{
		if (!other.CompareTag(this.tag))
		{
			Die();
		}
	}

	void Die()
	{
		if (OnDied != null)
		{
			OnDied();
		}

		projectileManager.UtilizeVisibleObjects();

		var obj = GameObject.Instantiate(explosionVFX, rigidbody.position, rigidbody.rotation);
		rigidbody.AddExplosionForce(5f, rigidbody.position, 2f);
		Destroy(gameObject);
	}
}