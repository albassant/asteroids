﻿using UnityEngine;
using System.Collections;

public class Projectile : DestroyableObject
{
	public float movementSpeed = 5f;

	void OnEnable()
	{
		rigidbody.velocity = transform.forward * movementSpeed;
		StartCoroutine(ObjectLifeHandler());
	}

	protected IEnumerator ObjectLifeHandler()
	{
		while (MainCamera.Instance.IsInsideVisibleArea(rigidbody.position))
		{
			yield return null;
		}

		if (OnLeftVisibleArea != null)
			OnLeftVisibleArea(this);
	}
}
