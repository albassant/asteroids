﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool<T> : LinkedList<T> where T : class
{
    int maxInstances = 10;
    int currentInstances = 0;

    public bool CanGrow = false;

    public ObjectPool(int maxInstances)
    {
        this.maxInstances = maxInstances;
    }

    public virtual bool Push(T obj)
    {
        if (currentInstances + 1 < maxInstances || CanGrow)
        {
            AddLast(obj);
            currentInstances++;
			return true;
        }
		return false;
    }

    public virtual T Instantiate()
    {
        if (First != null)
        {
            T ret = First.Value;
            RemoveFirst();
            currentInstances--;
            return ret;
        }

        return null;
    }
}

