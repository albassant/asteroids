﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectPool : ObjectPool<GameObject>
{
    public GameObject prefab;

    public GameObjectPool(int maxInstances) : base(maxInstances)
    {
    }

    public override bool Push(GameObject obj)
    {
		if (base.Push(obj))
		{
            obj.SetActive(false);
			return true;
		}

		GameObject.Destroy(obj);
		return false;
    }

    public GameObject Instantiate(Vector3 position, Quaternion rotation)
    {
        GameObject ret = base.Instantiate();

        if (ret == null && prefab != null)
        {
            ret = Object.Instantiate(prefab, position, rotation) as GameObject;
        }
        else
        {
            ret.transform.position = position;
            ret.transform.rotation = rotation;
            ret.SetActive(true);
        }

        return ret;
    }
}
