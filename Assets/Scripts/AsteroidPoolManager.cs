﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AsteroidPoolManager : DestroyableObjectPoolManager
{
	public System.Action<int> OnObjectDestroyedScore;

	public override DestroyableObject SpawnObject(Vector3 spawnPosition, Quaternion spawnRotation)
	{
		Asteroid obj = base.SpawnObject(spawnPosition, spawnRotation) as Asteroid;
		obj.OnSpawnParts += SpawnParts;

		return obj;
	}

	public void SpawnParts(Asteroid obj)
	{
		obj.OnSpawnParts -= SpawnParts;

		var part1 = (Asteroid)SpawnObject(obj.transform.position, obj.transform.rotation);
		part1.MaxDamage = Mathf.CeilToInt(obj.MaxDamage / 2f);

		var part2 = (Asteroid)SpawnObject(obj.transform.position, obj.transform.rotation);
		part2.MaxDamage = Mathf.CeilToInt(obj.MaxDamage / 2f);
	}

	protected override void UtilizeObject(DestroyableObject obj)
	{
		if (obj is Asteroid)
			(obj as Asteroid).OnSpawnParts -= SpawnParts;
		
		base.UtilizeObject(obj);
	}

	protected override void HandleDestruction(DestroyableObject obj)
	{
		if (OnObjectDestroyedScore != null && (obj is Asteroid))
		{
			int score = (obj as Asteroid).Score;
			OnObjectDestroyedScore(score);
		}
		
		base.HandleDestruction(obj);
	}
}
