﻿using UnityEngine;
using System.Collections;

public class DestroyableObject : MonoBehaviour
{
	public System.Action<DestroyableObject> OnCollision;
	public System.Action<DestroyableObject> OnLeftVisibleArea;

	protected Rigidbody rigidbody;

	void Awake()
	{
		rigidbody = GetComponent<Rigidbody>();
	}

	void OnTriggerEnter(Collider other)
	{
		if (!other.CompareTag(this.tag))
		{
			HandleDamage();
		}
	}

	protected virtual void HandleDamage()
	{
		if (OnCollision != null)
			OnCollision(this);
	}
}
