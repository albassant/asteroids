﻿using UnityEngine;
using System.Collections;

public class ExplosionLifeHandler : MonoBehaviour
{
	void Start()
	{
		float duration = GetComponent<ParticleSystem>().duration;
		Destroy(gameObject, duration);
	}
}
