﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
	/*
	 * TODO
	 * 1. game over
	 * 2. background
	 * 3. sounds
	 * 4. "explosion" bug: asteroid falls apart in 4 pieces instead of 2
	 * 5. asteroids flying to other galaxy
	 * 6. short projectile's life (from pool??)
	*/

	public enum EGameState
	{
		InProgress,
		Over,
	}
	private EGameState gameState;

	public System.Action OnGameIsOver;
	public System.Action<int> OnScoreUpdated;

	public PlayerController playerPrefab;

	private AsteroidPoolManager asteroidsManager;
	private PlayerController player;

	private int currentScore = 0;

	void Start()
	{
		asteroidsManager = GetComponent<AsteroidPoolManager>();
		if (asteroidsManager == null) Debug.LogAssertion("DestroyableObjectPoolManager component is missing");

		asteroidsManager.OnObjectDestroyedScore += (int score) => UpdateScore(currentScore + score);

		StartGame();
	}

	IEnumerator SpawnAsteroids()
	{
		while (gameState == EGameState.InProgress)
		{
			Vector3 spawnPoint = MainCamera.Instance.GetRandomEdgePosition();
			var asteroid = (Asteroid) asteroidsManager.SpawnObject(spawnPoint, Quaternion.identity);
			int[] damages = { 1, 2, 3 };
			asteroid.MaxDamage = damages[Random.Range(0, damages.Length)];

			float waitPeriod = Random.Range(0f, 10f);
			yield return new WaitForSeconds(waitPeriod);
		}
	}

	void FinishGame()
	{
		player.OnDied -= FinishGame;
		gameState = EGameState.Over;

		StopAllCoroutines();

		asteroidsManager.UtilizeVisibleObjects();

		if (OnGameIsOver != null)
			OnGameIsOver();
	}

	void UpdateScore(int newCurrentScore)
	{
		currentScore = newCurrentScore;
		if (OnScoreUpdated != null)
			OnScoreUpdated(currentScore);
	}

	public void StartGame()
	{
		player = Instantiate<PlayerController>(playerPrefab);
		player.OnDied += FinishGame;
		gameState = EGameState.InProgress;

		StartCoroutine(SpawnAsteroids());
	}
}
