﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour
{
    private static MainCamera instance;

    public static MainCamera Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<MainCamera>();
            return instance;
        }
    }

    private Vector3 bottomLeftCameraPoint;
    private Vector3 topRightCameraPoint;
    private Vector3 worldCenter;
    private Vector2 worldDimensions;

    public Vector2 WorldDimensions { get { return worldDimensions; } }
    public Vector3 WorldCenter { get { return worldCenter; } }

    void Awake()
    {
        bottomLeftCameraPoint = Camera.main.ViewportToWorldPoint(Vector3.zero);
        topRightCameraPoint = Camera.main.ViewportToWorldPoint(Vector3.one);
        worldCenter = Camera.main.ViewportToWorldPoint(Vector3.one * 0.5f);

        bottomLeftCameraPoint.y = topRightCameraPoint.y = worldCenter.y = 0f;

        worldDimensions = new Vector2(topRightCameraPoint.x - bottomLeftCameraPoint.x, topRightCameraPoint.z - bottomLeftCameraPoint.z);
    }

    public bool IsInsideVisibleArea(Vector3 point)
    {
        return point.x > bottomLeftCameraPoint.x && point.x < topRightCameraPoint.x
            && point.z > bottomLeftCameraPoint.z && point.z < topRightCameraPoint.z;
    }

	public Vector3 InvertPosition(Vector3 point)
	{
		point.x = -point.x;
		point.z = -point.z;

		return point;
	}

	public Vector3 GetRandomEdgePosition()
	{
		Vector2 res = Random.insideUnitCircle;
		float radius = Mathf.Max(topRightCameraPoint.x, topRightCameraPoint.z);
		res = res.normalized * radius;
		return new Vector3(res.x, 0f, res.y);
	}
}
