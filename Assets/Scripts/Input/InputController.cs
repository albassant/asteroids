﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    public System.Action<bool> OnFireButtonPressed;
    public System.Action<bool> OnLeftButtonPressed;
    public System.Action<bool> OnRightButtonPressed;
    public System.Action<bool> OnAccelerationButtonPressed;

    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (OnFireButtonPressed != null)
                OnFireButtonPressed(true);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (OnFireButtonPressed != null)
                OnFireButtonPressed(false);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (OnLeftButtonPressed != null)
                OnLeftButtonPressed(true);
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            if (OnLeftButtonPressed != null)
                OnLeftButtonPressed(false);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (OnRightButtonPressed != null)
                OnRightButtonPressed(true);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            if (OnRightButtonPressed != null)
                OnRightButtonPressed(false);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (OnAccelerationButtonPressed != null)
                OnAccelerationButtonPressed(true);
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (OnAccelerationButtonPressed != null)
                OnAccelerationButtonPressed(false);
        }
    }
}
