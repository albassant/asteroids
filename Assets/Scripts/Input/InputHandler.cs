﻿using UnityEngine;
using System.Collections;

public class InputHandler : MonoBehaviour
{
    InputController inputController;
    PlayerController player;

    void Start()
    {
        inputController = FindObjectOfType<InputController>();
        if (inputController == null) Debug.LogError("No InputController found!");

        inputController.OnFireButtonPressed += OnFireButtonPressed;
        inputController.OnLeftButtonPressed += OnLeftButtonPressed;
        inputController.OnRightButtonPressed += OnRightButtonPressed;
        inputController.OnAccelerationButtonPressed += OnAccelerationButtonPressed;

        player = GetComponent<PlayerController>();
        if (player == null) Debug.LogError("No PlayerController component found!");
    }

    void OnFireButtonPressed(bool pressed)
    {
        player.Fire();
    }

    void OnLeftButtonPressed(bool pressed)
    {
        var rotDir = pressed ? PlayerController.ERotationDirection.Left : PlayerController.ERotationDirection.None;
        player.Rotate(rotDir);
    }

    void OnRightButtonPressed(bool pressed)
    {
        var rotDir = pressed ? PlayerController.ERotationDirection.Right : PlayerController.ERotationDirection.None;
        player.Rotate(rotDir);
    }

    void OnAccelerationButtonPressed(bool pressed)
    {
        player.Accelerate(pressed);
    }

	void OnDestroy()
	{
		if (inputController != null)
		{
			inputController.OnFireButtonPressed -= OnFireButtonPressed;
			inputController.OnLeftButtonPressed -= OnLeftButtonPressed;
			inputController.OnRightButtonPressed -= OnRightButtonPressed;
			inputController.OnAccelerationButtonPressed -= OnAccelerationButtonPressed;
		}
	}
}
